import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  constructor(private angularFireStore: AngularFirestore) { }

  collection$(path,query?) {
    return this.angularFireStore
      .collection(path, query)
      .snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(a => {
            const data: Object = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        })
      )
  }

  deleteDocument(path) : Promise<any> {
    return this.angularFireStore
      .doc(path)
      .delete()
  }

  doc$(path) : Observable<any> {
    return this.angularFireStore
      .doc(path)
      .snapshotChanges()
      .pipe(
        map(doc => {
          const data: Object = doc.payload.data();
          const id = doc.payload.id;
          return { id: id, ...data };
        })
      )
  }

  checkDocument(collection: string, document: string) : Observable<any> {
    return this.angularFireStore
      .collection(collection)
      .doc(document)
      .get();
  }

  updateAt(path: string, data: Object): Promise<any> {
    const segments = path.split('/').filter(v => v);
    if(segments.length % 2) {
      return this.angularFireStore.collection(path).add(data);
    } else {
      return this.angularFireStore.doc(path).set(data, {merge: true});
    }
  }
}
