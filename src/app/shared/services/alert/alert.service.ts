import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    public alertController: AlertController
  ) { }
  
  async presentAlert(header: string, subHeader?: string, message?: string) {
    const alert = await this.alertController.create({
      header: `${header}`,
      subHeader: `${subHeader}`,
      message: `${message}`,
      buttons: ['OK']
    });

    return await alert;
  }
  
  async presentPromptWithInput(header: string, inputs: any, handler: any, subHeader?: string, message?: string) {
    const alert = await this.alertController.create({
      header: `${header}`,
      subHeader: `${subHeader}`,
      message: `${message}`,
      inputs: inputs,
      buttons: [
        {
          text: 'Done',
          handler: data => handler(data)
        }
      ]
    });
    return await alert;
  }
}
