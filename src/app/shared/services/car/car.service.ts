import { Injectable } from '@angular/core';
import { DatabaseService } from '../database/database.service';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(
    private databaseService: DatabaseService
  ) { }
  
  async getCars() {
    return this.databaseService.collection$(
      `fruit`, ref =>
      ref
        .orderBy('Created_date', 'desc')
    );
  }

  async editCar(car: any) {
    return this.databaseService.updateAt(`fruit/${car.id}`,car);
  }

  async deleteCar(carId: string) {
    return this.databaseService.deleteDocument(`fruit/${carId}`);
  }
}
