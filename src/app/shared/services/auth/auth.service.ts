import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user$: Observable<any>;
  constructor(
    private angularFireAuth: AngularFireAuth
    ) { 
      this.user$ = this.angularFireAuth.authState;
    }

    signIn (email: string, password: string) {
      return this.angularFireAuth.signInWithEmailAndPassword(email,password);
    }

    signUp (email: string, password: string) {
      return this.angularFireAuth.createUserWithEmailAndPassword(email, password);
    }
}
