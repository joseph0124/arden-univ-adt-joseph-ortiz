export interface Fruit {
    Make: string;
    Max_speed: number;
    Engine_size: number;
    Created_at: any;
}