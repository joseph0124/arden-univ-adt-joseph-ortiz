import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from '../shared/services/alert/alert.service';
import { AuthService } from '../shared/services/auth/auth.service';
import { LoadingService } from '../shared/services/loading/loading.service';
import { ToastService } from '../shared/services/toast/toast.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public passwordType: string;
  public passwordShown: boolean;
  public passwordIcon: string;
  public password: string;
  public email: string;

  constructor(
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService,
    private toastService: ToastService,
    private loadingService: LoadingService
  ) {
    this.passwordType = "password";
    this.passwordShown = false;
    this.passwordIcon = "eye";
    this.email = "";
    this.password = "";
  }

  async onLogin() {
    let loading = await this.loadingService.presentLoading("Signing In...");
    loading.present();
    this.email = this.email.toLowerCase();
    this.authService.signIn(this.email, this.password)
      .then(async () => {
        this.router.navigate(['cars-list']);
        let toast = await this.toastService.presentToast("Login Successful!");
        toast.present();
        loading.dismiss();
      }).catch(async (error) => {
        loading.dismiss();
        if (error.code === "auth/user-not-found") {
          let handler = async (data) => {
            let loadingSignUp = await this.loadingService.presentLoading("Signing Up...");
            loadingSignUp.present();
            this.authService.signUp(this.email, this.password).then(async (response) => {
              let user = response.user;
              user.updateProfile({ displayName: data.name }).then(async () => {
                loadingSignUp.dismiss();
                this.router.navigate(['cars-list']);
                let toast = await this.toastService.presentToast("Signup Successful!");
                toast.present();
              }).catch(async (error) => {
                loadingSignUp.dismiss();
                let alert = await this.alertService.presentAlert('Signup Failed', '', error.message);
                alert.present();
              });
            }).catch(async (error) => {
              loadingSignUp.dismiss();
              let alert = await this.alertService.presentAlert('Signup Failed', '', error.message);
              alert.present();
            })
          }
          let inputs = [
            {
              name: 'name',
              type: 'text',
              label: 'Name'
            }
          ];
          let namePrompt = await this.alertService.presentPromptWithInput("Enter Name", inputs, handler, "", "");
          namePrompt.present();
        } else {
          let alert = await this.alertService.presentAlert('Login Failed', '', error.message);
          alert.present();
        }
      });
  }
  
  onTogglePassword() {
    this.passwordShown = !this.passwordShown;
    this.passwordType = this.passwordShown ? 'text' : 'password';
    this.passwordIcon = this.passwordShown ? 'eye-off' : 'eye';
  }
}
