import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../shared/services/auth/auth.service';
import { CarService } from '../shared/services/car/car.service';

@Component({
  selector: 'app-cars-list',
  templateUrl: './cars-list.page.html',
  styleUrls: ['./cars-list.page.scss'],
})
export class CarsListPage implements OnInit {

  public cars$: Observable<any>;

  constructor(
    public authService: AuthService,
    private carService: CarService
  ) { }

  async ngOnInit() {
    this.cars$ = await this.carService.getCars();
    this.cars$.subscribe((response) => {
      console.log(response)
    })
  }

  onDelete(carId: string) {
    this.carService.deleteCar(carId);
  }
  onEdit(car: any) {
    this.carService.editCar(car);
  }
  
}
