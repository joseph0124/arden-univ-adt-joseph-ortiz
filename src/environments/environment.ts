// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDLKXHKp1U9O5IZ8J8Gqmw5A2vFnt6qZsM",
    authDomain: "arden-univ-adt-joseph-ortiz.firebaseapp.com",
    databaseURL: "https://arden-univ-adt-joseph-ortiz.firebaseio.com",
    projectId: "arden-univ-adt-joseph-ortiz",
    storageBucket: "arden-univ-adt-joseph-ortiz.appspot.com",
    messagingSenderId: "703169915206",
    appId: "1:703169915206:web:6325439e506da501ae1d47"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
